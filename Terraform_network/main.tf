terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABqT1WnAATuwQAAAADiI_aB6YwmclAmR9aXAPWsDf3Bkut5-lk"
  cloud_id  = "b1go4n53hofro0ufj8ji"
  folder_id = "b1gj2tad8qm148k1ihre"
  zone      = "ru-central1-a"
}

resource "yandex_vpc_network" "vpc" {
  name = "vpc"
}

resource "yandex_vpc_subnet" "network-sn-1" {
  name           = "dmz"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = ["10.10.0.0/24"]
}

resource "yandex_vpc_subnet" "network-sn-2" {
  name           = "app1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = ["10.10.10.0/24"]
}

resource "yandex_vpc_subnet" "network-sn-3" {
  name           = "app2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = ["10.10.20.0/24"]
}

resource "yandex_vpc_subnet" "network-sn-4" {
  name           = "db1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = ["10.100.0.0/24"]
}

resource "yandex_vpc_subnet" "network-sn-5" {
  name           = "db2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc.id
  v4_cidr_blocks = ["10.200.0.0/24"]
}

resource "yandex_vpc_security_group" "my-sg-1" {
  name        = "sg-dmz"
  network_id  = yandex_vpc_network.vpc.id

  ingress {
      protocol       = "TCP"
      v4_cidr_blocks = ["0.0.0.0/0"]
      port           = 80
  }

  egress {
      protocol       = "TCP"
      v4_cidr_blocks = ["0.0.0.0/0"]
      port           = 80
 }

}  

resource "yandex_vpc_security_group" "my-sg-2" {
  name        = "sg-app1"
  network_id  = yandex_vpc_network.vpc.id

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.0.0/24"]
      port           = 80
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.0.0/24"]
      port           = 80
 }

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24", "10.10.20.0/24", "10.100.0.0/24"]
      from_port           = 0
      to_port             = 65535
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24", "10.10.20.0/24", "10.100.0.0/24"]
      from_port           = 0
      to_port             = 65535
  }
}

resource "yandex_vpc_security_group" "my-sg-3" {
  name        = "sg-app2"
  network_id  = yandex_vpc_network.vpc.id

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.0.0/24"]
      port           = 80
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.0.0/24"]
      port           = 80
 }

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24", "10.10.20.0/24", "10.200.0.0/24"]
      from_port           = 0
      to_port             = 65535
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24", "10.10.20.0/24", "10.200.0.0/24"]
      from_port           = 0
      to_port             = 65535
  }
}

resource "yandex_vpc_security_group" "my-sg-4" {
  name        = "sg-db1"
  network_id  = yandex_vpc_network.vpc.id

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24"]
      from_port           = 0
      to_port             = 65535
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.10.0/24"]
      from_port           = 0
      to_port             = 65535
  }
}

resource "yandex_vpc_security_group" "my-sg-5" {
  name        = "sg-db2"
  network_id  = yandex_vpc_network.vpc.id

  ingress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.20.0/24"]
      from_port           = 0
      to_port             = 65535
  }

  egress {
      protocol       = "ANY"
      v4_cidr_blocks = ["10.10.20.0/24"]
      from_port           = 0
      to_port             = 65535
  }
}
