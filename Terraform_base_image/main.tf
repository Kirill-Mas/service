terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "y0_AgAAAABqT1WnAATuwQAAAADiI_aB6YwmclAmR9aXAPWsDf3Bkut5-lk"
  cloud_id  = "b1go4n53hofro0ufj8ji"
  folder_id = "b1gj2tad8qm148k1ihre"
  zone      = "ru-central1-a"
}

resource "yandex_compute_image" "image-1" {
  name        = "base-prod"
  source_disk = "fhm513dsdjm47pn86jnt"
}
